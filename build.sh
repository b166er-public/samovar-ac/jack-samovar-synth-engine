#!/bin/bash

# Clear & Create the target folder
rm -rf target
mkdir target

# Compile the generator
INCLUDE_FOLDERS="./include"
gcc -I${INCLUDE_FOLDERS} -g -c ./src/jsse.c -o ./target/jsse.o -pthread
gcc -I${INCLUDE_FOLDERS} -g -c ./src/example.c -o ./target/example.o -pthread

# Link the generator
OBJ_FILES="./target/jsse.o"
OBJ_FILES="${OBJ_FILES} ./target/example.o"

gcc -g -o ./target/example -ljack -lm -pthread ${OBJ_FILES}

# Print the environment
# printenv

# Make generator executable
chmod +x ./target/example
