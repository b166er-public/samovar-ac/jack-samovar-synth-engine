/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

// ----------------------------------------------------------------------------
// Basic definitions
// ----------------------------------------------------------------------------

#define MIDI_CHANNELS 16
#define MIDI_KEYS 128
#define MIDI_CONTROLS 128

#define ENV_VARIABLE_JACK_CLIENT_NAME "JSSE_JACK_CLIENT_NAME"

// ----------------------------------------------------------------------------
// JSSE CV Bus state (Structure & Methods)
// ----------------------------------------------------------------------------

typedef unsigned char uint8_t;

typedef struct jsse_cv_channel_state {
    uint8_t key_states[MIDI_KEYS];
    uint8_t control_states[MIDI_CONTROLS];
} jsse_cv_channel_state_t;

typedef struct jsse_cv_bus_state {
    jsse_cv_channel_state channel_states[MIDI_CHANNELS];
} jsse_cv_bus_state_t;

typedef struct jsse_cv_channel_delta {
    bool equal;
    bool keys[MIDI_KEYS];
    bool controls[MIDI_CONTROLS];
} jsse_cv_channel_delta_t;

void jsse_cv_bus_state_create(jsse_cv_bus_state_t* obj);

void jsse_cv_bus_state_set_key(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t key, 
    uint8_t value
);

void jsse_cv_bus_state_set_control(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t control, 
    uint8_t value
);

uint8_t jsse_cv_bus_state_get_key(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t key
);

uint8_t jsse_cv_bus_state_get_control(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t control
);

void jsse_cv_bus_derive_channel_delta(
    jsse_cv_bus_state_t* t0,
    jsse_cv_bus_state_t* t1,
    uint8_t channel,
    jsse_cv_channel_delta_t* result
);

// ----------------------------------------------------------------------------
// JSSE Instrument interface
// ----------------------------------------------------------------------------

typedef void* instrument_ptr;

typedef instrument_ptr (*jsse_instrument_create_callback) (
    unsigned int sampling_rate,
    unsigned int buffer_size
);

typedef void (*jsse_instrument_process_callback) (
    instrument_ptr obj,
    unsigned int sampling_rate,
    unsigned int buffer_size,
    jsse_cv_bus_state_t* midi_in,
    jsse_cv_bus_state_t* midi_out,
    float* pcm_left_out,
    float* pcm_right_out
);

typedef void (*jsse_instrument_destroy_callback) (
    instrument_ptr obj
);

// ----------------------------------------------------------------------------
// JSSE Runtime
// ----------------------------------------------------------------------------

void jsse_runtime_start(
    const char*                         default_jack_client_name,
    jsse_instrument_create_callback     instrument_create_callback,
    jsse_instrument_process_callback    instrument_process_callback,
    jsse_instrument_destroy_callback    instrument_destroy_callback
);