/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <jack/jack.h>
#include <jack/midiport.h>

#include "jsse.h"

// ----------------------------------------------------------------------------
// Common internal functions
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// JSSE CV Bus state (Methods implementation)
// ----------------------------------------------------------------------------

void 
jsse_cv_bus_state_create(jsse_cv_bus_state_t* obj) {
    assert(obj != 0);
    memset(
        obj, 
        0, 
        sizeof(jsse_cv_bus_state_t)
    );
}

void 
jsse_cv_bus_state_set_key(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t key, 
    uint8_t value
) {
    assert(obj != 0);
    (&obj).channel_states[channel].key_states[key] = value;
}

void 
jsse_cv_bus_state_set_control(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t control, 
    uint8_t value
) {
    assert(obj != 0);
    (&obj).channel_states[channel].control_states[control] = value;
}

uint8_t 
jsse_cv_bus_state_get_key(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t key
) {
    assert(obj != 0);
    return (&obj).channel_states[channel].key_states[key];
}

uint8_t 
jsse_cv_bus_state_get_control(
    jsse_cv_bus_state_t* obj, 
    uint8_t channel, 
    uint8_t control
) {
    assert(obj != 0);
    return (&obj).channel_states[channel].control_states[control];
}

void 
jsse_cv_bus_derive_channel_delta(
    jsse_cv_bus_state_t* t0,
    jsse_cv_bus_state_t* t1,
    uint8_t channel,
    jsse_cv_channel_delta_t* result
) {
    assert(t0 != 0);
    assert(t1 != 0);
    assert(result != 0);
    
    // Reset result
    memset(
        result, 
        0, 
        sizeof(jsse_cv_channel_delta_t)
    );
    
    // Calculate result
    jsse_cv_channel_state_t t0_chn = t0->channel_states[channel];
    jsse_cv_channel_state_t t1_chn = t1->channel_states[channel];
    
    result->equal = true;
    
    for(int k=0; k < MIDI_KEYS; k++) {
        if(t0_chn.key_states[k] != t1_chn.key_states[k]) {
            result->keys[k] = true;
            result->equal = false;
        }
    }
    
    for(int c=0; c < MIDI_CONTROLS; c++) {
        if(t0_chn.control_states[c] != t1_chn.control_states[c]) {
            result->controls[k] = true;
            result->equal = false;
        }
    }
}

// ----------------------------------------------------------------------------
// JSSE Runtime
// ----------------------------------------------------------------------------

static
void
jsse_log_info(const char* function, const char* message) {
    fprintf(stdout, "[I] [%s] %s\n", function, message);
}

static
void
jsse_log_warning(const char* function, const char* message) {
    fprintf(stdout, "[W] [%s] %s\n", function, message);
}

static 
void
jsse_log_error(const char* function, const char* message) {
    fprintf(stderr, "[E] [%s] %s\n", function, message);
}

typedef struct jsse_runtime {
    // Common variables
    unsigned int samping_rate;
    unsigned int buffer_size;
    
    // JACK resources
    jack_client_t* jack_client;
    jack_port_t* jack_port_midi_in;
    jack_port_t* jack_port_midi_out;
    jack_port_t* jack_port_pcm_left_out;
    jack_port_t* jack_port_pcm_right_out;
    
    // Buffer (MIDI & PCM)
    jsse_cv_bus_state_t cv_state_in;
    jsse_cv_bus_state_t cv_state_out;
    float* pcm_left;
    float* pcm_right;
    
    // Provider thread
    pthread_t provider_thread;
    
    // Synchronization mutex
    pthread_mutex_t generation_granted;
    pthread_mutex_t consuming_granted;
    
    // Killer mutex
    pthread_mutex_t killer_mutex;
    
} jsse_runtime_t;


// Consuming function (invoked by JACK)
static
int
jsse_runtime_consumer(
    jack_nframes_t nframes,
    void *arg
) {
    assert(nframes > 0);
    assert(arg != 0);
    
    jsse_runtime_t* obj = (jsse_runtime_t*) arg;
    
    pthread_mutex_lock(&obj->consuming_granted);
    // TODO
    pthread_mutex_unlock(&obj->generation_granted);
    
    return 0;
}

// Provider function (run as soon as next buffer can be calculated)
static
void*
jsse_runtime_provider(void *arg) {
    assert(arg != 0);
    
    jsse_runtime_t* obj = (jsse_runtime_t*) arg;
    
    pthread_mutex_lock(&obj->generation_granted);
    // TODO
    pthread_mutex_unlock(&obj->consuming_granted);
    
    return NULL;
}


void 
jsse_runtime_start(
    const char*                         default_jack_client_name,
    jsse_instrument_create_callback     instrument_create_callback,
    jsse_instrument_process_callback    instrument_process_callback,
    jsse_instrument_destroy_callback    instrument_destroy_callback
) {
    jsse_runtime_t obj;
    
    // Derive JACK client name
    const char* jack_client_name = NULL;
    if(getenv(ENV_VARIABLE_JACK_CLIENT_NAME)) {
        jack_client_name = getenv(ENV_VARIABLE_JACK_CLIENT_NAME);
    } else {
        jack_client_name = default_jack_client_name;
    }
    
    // Open JACK client
    obj.jack_client = jack_client_open(
        jack_client_name, /* client name*/
        JackNullOption, /* options */
        NULL, /* status */
        NULL /* server name */
    );
    
    // Create MIDI IN Port
    obj.jack_port_midi_in = jack_port_register(
        obj.jack_client, 
        "midi_in",
        JACK_DEFAULT_MIDI_TYPE,
        JackPortIsInput, 
        0
    );

    // Create MIDI IN Port
    obj.jack_port_midi_out = jack_port_register(
        obj.jack_client, 
        "midi_out",
        JACK_DEFAULT_MIDI_TYPE,
        JackPortIsOutput, 
        0
    );
    
    // Create PCM OUT Port (left)
    obj.jack_port_pcm_left_out = jack_port_register(
        obj->jack_client_handle, 
        "pcm_out_left",
        JACK_DEFAULT_AUDIO_TYPE,
        JackPortIsOutput, 
        0
    );
    
    // Create PCM OUT Port (right)
    obj.jack_port_pcm_left_out = jack_port_register(
        obj->jack_client_handle, 
        "pcm_out_right",
        JACK_DEFAULT_AUDIO_TYPE,
        JackPortIsOutput, 
        0
    );

    // TODO
    

}