/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jsse.h"

void* example_create_callback(
    unsigned int sampling_rate,
    unsigned int buffer_size
) {
    return NULL;
}

void example_process_callback(
    instrument_ptr obj,
    unsigned int sampling_rate,
    unsigned int buffer_size,
    jsse_cv_bus_state_t* midi_in,
    jsse_cv_bus_state_t* midi_out,
    float* pcm_left_out,
    float* pcm_right_out
) {
    return;
}

void example_destroy_callback(
    instrument_ptr obj
) {
    return;
}

int main(int argc, char* argv[]) {
    jsse_runtime_start(
        "example",
        &example_create_callback,
        &example_process_callback,
        &example_destroy_callback
    );
    return 0;
}