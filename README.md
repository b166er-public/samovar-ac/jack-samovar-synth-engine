# Jack Samovar Synth Engine

A synthesizer engine which supports multiple instruments

## Design draft

What follows is a semi-structured design draft which describe how the Jack Samovar Synth Engine (JSSE) will look like.

### CV State structure

The control voltage will be derived from MIDI events. The structure will be called ```jsse_cv_bus_state```. The layout will be something like this:

```c
#define MIDI_CHANNELS 16
#define MIDI_KEYS 128
#define MIDI_CONTROLS 128

typedef char uint8;

typedef struct jsse_cv_channel_state {
    uint8 key_states[MIDI_KEYS];
    uint8 control_states[MIDI_CONTROLS];
} jsse_cv_channel_state_t;

typedef struct jsse_cv_bus_state {
    jsse_cv_channel_state channel_states[MIDI_CHANNELS];
} jsse_cv_bus_state_t;
```

Following methods will be provided for the ```jsse_cv_bus_state```:

```c
void jsse_cv_bus_state_create(jsse_cv_bus_state_t* obj);

void jsse_cv_bus_state_set_key(
    jsse_cv_bus_state_t* obj, 
    uint8 channel, 
    uint8 key, 
    uint8 value
);

void jsse_cv_bus_state_set_control(
    jsse_cv_bus_state_t* obj, 
    uint8 channel, 
    uint8 control, 
    uint8 value
);

uint8 jsse_cv_bus_state_get_key(
    jsse_cv_bus_state_t* obj, 
    uint8 channel, 
    uint8 key
);

uint8 jsse_cv_bus_state_get_control(
    jsse_cv_bus_state_t* obj, 
    uint8 channel, 
    uint8 control
);
```

For efficient delta calculation per MIDI channel we define following method:

```c
typedef struct jsse_cv_channel_delta {
    bool keys[MIDI_KEYS];
    bool controls[MIDI_CONTROLS];
} jsse_cv_channel_delta_t;

void jsse_cv_bus_derive_channel_delta(
    jsse_cv_bus_state_t* t0,
    jsse_cv_bus_state_t* t1,
    uint8 channel,
    jsse_cv_channel_delta_t* result
);
```

### Instrument definition

The engine is only a lightweight wrapper which provides an abstraction for an instrument. For JSSE an instrument is defined by only tree methods:

```c
typedef void* instrument_handler_ptr;

typedef instrument_handler_ptr (*jsse_instrument_create_callback) (
    unsigned int sampling_rate,
    unsigned int buffer_size
);

typedef void (*jsse_instrument_process_callback) (
    instrument_handler_ptr obj,
    unsigned int sampling_rate,
    unsigned int buffer_size,
    jsse_cv_bus_state_t* midi_in,
    jsse_cv_bus_state_t* midi_out,
    float* pcm_left_out,
    float* pcm_right_out
);

typedef void (*jsse_instrument_destroy_callback) (
    instrument_handler_ptr obj
);

```

### Thread model

The engine works on a very primitive thread model.

* **Consumer Thread** is the thread started by JACK Audio and is responsible for the following tasks
  * Copy last calculate PCM buffer to the location where JACK wanted it to be
  * Update the MIDI IN bus state by reading all received messages and set the new values in the corresponding bus state
  * Update the MIDI OUT bus state by reading all calculated messages and put them on the MIDI OUT bus
* **Provider Thread** is the thread started by JSSE and it responsible for the following tasks
  * Intialize the instrument handler by calling the corresposing callback (only once)
  * Calculate the content of the PCM buffer which should be transfered next time to JACK
    * Done by calling the process handler of the instrument (done every cycle)
  * Destroy the instrument handler if SIGTERM signal had been received

### Thread synchronization strategy

TODO: Show you mutex idea

### Engine handler structure

TODO: Write down here all variables which will be needed during the session

### Jack autoconnection configuration

TODO: Which environment variables should be set to autoconnect to certain JACK ports
